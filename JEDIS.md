<h1 align="center">Sponsors &amp; Backers</h1>

**R2Devops** is a **collaborative** and **open source** platform, allowing developers to generate in one click a pipeline that automates development supply-chain **painful tasks**.

None of this will be possible without our contributors and sponsors 👇