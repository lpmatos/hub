# Changelog
All notable changes to this job will be documented in this file.

## [0.2.0] - 2022-09-20
* Enable pages composability by default

## [0.1.0] - 2022-07-11
* Initial version
